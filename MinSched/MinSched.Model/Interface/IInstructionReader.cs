﻿using System;
using System.Collections.Generic;
using MinSched.Model.Entities;

namespace MinSched.Model.Interface
{
    public interface IInstructionReader : IDisposable
    {
        IEnumerable<Instruction> Read();
    }
}