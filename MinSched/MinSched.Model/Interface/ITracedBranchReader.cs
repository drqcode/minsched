﻿using System;
using System.Collections.Generic;
using MinSched.Model.Entities;

namespace MinSched.Model.Interface
{
    public interface ITracedBranchReader : IDisposable
    {
        IList<TracedBranch> Read();
    }
}