﻿using System;
using System.Collections.Generic;
using MinSched.Model.Entities;

namespace MinSched.Model.Interface
{
    public interface IInstructionSerializer: IDisposable
    {
        void WriteBlock(IEnumerable<Instruction> instructions);
        void WriteInstruction(Instruction instruction);
    }
}