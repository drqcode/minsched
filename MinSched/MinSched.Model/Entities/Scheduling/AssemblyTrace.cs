using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MinSched.Infrastructure;
using MinSched.Model.DomainBuilders;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Interface;

namespace MinSched.Model.Entities.Scheduling
{
    public class AssemblyTrace
    {
        private readonly string _name;
        protected IList<Instruction> InstructionList;

        public AssemblyTrace(string name, AssemblyTraceBuilder builder)
        {
            _name = name;
            InstructionList = builder.GetInstructions().ToList();
        }

        public AssemblyTrace(AssemblyTrace trace)
        {
            _name = trace.Name;
            InstructionList = new List<Instruction>();
            foreach (var traceAssemblyInstruction in trace.AssemblyInstructions)
            {
                InstructionList.Add(traceAssemblyInstruction.Clone());
            }
        }

        public AssemblyTrace(string name)
        {
            _name = name;
            InstructionList = new List<Instruction>();
            LoadTrace();
        }

        public string Name => _name;
        public IEnumerable<Instruction> AssemblyInstructions => InstructionList;
        public int NumberOfInstructions => InstructionList.Count;

        public void LoadTrace()
        {
            var loader = new FileToTraceLoader(_name);
            InstructionList = loader.Read().ToList();
            loader.Dispose();
        }

        public virtual void SaveTrace()
        {
            var serializer = new TraceToFileSerializer(_name);
            serializer.WriteBlock(InstructionList);
            serializer.Dispose();
        }

        private class FileToTraceLoader : IInstructionReader
        {
            private readonly StreamReader _reader;
            private readonly string _btrcFileName;
            private readonly string _btrcFilePath;

            public FileToTraceLoader(string name)
            {
                _btrcFileName = Path.ChangeExtension(name, ".btrc");
                _btrcFilePath = UserFileManager.Instance.GetFilePath(_btrcFileName);
                _reader = new StreamReader(_btrcFilePath);
            }

            public void Dispose()
            {
                _reader.Close();
            }

            public IEnumerable<Instruction> Read()
            {
                while (!_reader.EndOfStream)
                {
                    var rawLine = _reader.ReadLine();

                    var opcode = rawLine.Substring(0, rawLine.IndexOf(' '));
                    var parts = rawLine.Substring(rawLine.IndexOf(' ') + 1).Trim().Split(',');
                    var operands = new string[3];

                    for (var i = 0; i < parts.Length; i++)
                    {
                        parts[i] = parts[i].Trim();
                        if (!string.IsNullOrWhiteSpace(parts[i]))
                        {
                            operands[i] = parts[i];
                        }
                    }

                    var instruction = new Instruction(AssemblyCode.OpcodeMapper.Map(opcode))
                    {
                        Destination = operands[0],
                        Source1 = operands[1],
                        Source2 = operands[2]
                    };
                    yield return instruction;
                }
            }
        }

        private class TraceToFileSerializer : IInstructionSerializer
        {
            private readonly StreamWriter _streamWriter;

            public TraceToFileSerializer(string name)
            {
                var btrcFileName = Path.ChangeExtension(name, ".btrc");
                var btrcFilePath = UserFileManager.Instance.GetFilePath(btrcFileName);
                _streamWriter = new StreamWriter(btrcFilePath, false);
            }

            public void WriteBlock(IEnumerable<Instruction> instructions)
            {
                var list = instructions.ToList();
                for (var i = 0; i < list.Count-1; i++)
                {
                    _streamWriter.WriteLine(list[i]);
                }
                _streamWriter.Write(list.Last());
            }

            public void Dispose()
            {
                _streamWriter.Close();
            }

            public void WriteInstruction(Instruction instruction)
            {
                throw new NotImplementedException();
            }
        }
    }
}