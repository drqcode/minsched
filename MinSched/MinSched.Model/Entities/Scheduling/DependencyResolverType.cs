namespace MinSched.Model.Entities.Scheduling
{
    public enum DependencyResolverType
    {
        NoDependency,
        TrueDependency,
        AntiAliasMemory,
        ImmediateMerging,
        MovMerging,
        MovReabsorption
    }
}
