namespace MinSched.Model.Entities.Scheduling
{
    public struct TrueDependency
    {
        public int InstructionAddress1 => _instructionAddress1;
        public int InstructionAddress2 => _instructionAddress2;
        public DependencyResolverType DependencyResolverType => _dependencyResolverType;

        private readonly int _instructionAddress1;
        private readonly int _instructionAddress2;
        private readonly DependencyResolverType _dependencyResolverType;

        public TrueDependency(int instructionAddress1, int instructionAddress2, DependencyResolverType dependencyResolverType)
        {
            _instructionAddress1 = instructionAddress1;
            _instructionAddress2 = instructionAddress2;
            _dependencyResolverType = dependencyResolverType;
        }
    }
}