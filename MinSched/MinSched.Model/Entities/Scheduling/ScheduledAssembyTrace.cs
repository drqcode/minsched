using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MinSched.Infrastructure;
using MinSched.Model.DomainBuilders;
using MinSched.Model.Interface;

namespace MinSched.Model.Entities.Scheduling
{
    public class ScheduledAssembyTrace : AssemblyTrace
    {
        private ConcurrentBag<TrueDependencyGraph> _dependencyGraphs;
        private int _windowSize;

        #region Properties
        public int WindowSize
        {
            get { return _windowSize; }
            set
            {
                if (value < 1 || value > NumberOfInstructions)
                {
                    throw new Exception("Window size out of range!");
                }
                _windowSize = value;
            }


        }
        public ConcurrentBag<TrueDependencyGraph> DependencyGraphs => _dependencyGraphs;
        #endregion

        #region Constructors
        public ScheduledAssembyTrace(string name, AssemblyTraceBuilder builder) : base(name, builder)
        {
        }

        //window default size = 1
        public ScheduledAssembyTrace(AssemblyTrace trace, int windowSize = 1) : base(trace)
        {
            WindowSize = windowSize;
            _dependencyGraphs = new ConcurrentBag<TrueDependencyGraph>();
        }
        #endregion

        public IList<Instruction> GetWindow(int windowAddress)
        {
            var endAddress = windowAddress + WindowSize;
            if (endAddress > NumberOfInstructions)
            {
                endAddress = NumberOfInstructions;
            }
            var instructions = new List<Instruction>();
            for (var i = windowAddress; i < endAddress; i++)
            {
                instructions.Add(InstructionList[i]);
            }
            return instructions;
        }

        public override void SaveTrace()
        {
            var serializer = new TraceToFileSerializer(Name);
            serializer.WriteBlock(InstructionList);
            serializer.Dispose();
        }

        private class TraceToFileSerializer : IInstructionSerializer
        {
            private readonly StreamWriter _streamWriter;

            public TraceToFileSerializer(string name)
            {
                var btrcFileName = Path.ChangeExtension(name, ".strc");
                var btrcFilePath = UserFileManager.Instance.GetFilePath(btrcFileName);
                _streamWriter = new StreamWriter(btrcFilePath, false);
            }

            public void WriteBlock(IEnumerable<Instruction> instructions)
            {
                var list = instructions.ToList();
                for (var i = 0; i < list.Count - 1; i++)
                {
                    _streamWriter.WriteLine(list[i]);
                }
                _streamWriter.Write(list.Last());
            }

            public void Dispose()
            {
                _streamWriter.Close();
            }

            public void WriteInstruction(Instruction instruction)
            {
                throw new NotImplementedException();
            }
        }
    }
}