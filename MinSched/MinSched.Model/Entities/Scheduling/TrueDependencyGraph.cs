using System;
using System.Collections.Generic;
using System.ComponentModel.Design;

namespace MinSched.Model.Entities.Scheduling
{
    public class TrueDependencyGraph
    {
        private int _graphSize;
        private int[] _predecessor;


        public TrueDependencyGraph(int windowSize)
        {
            _graphSize = windowSize;

            _predecessor = new int[_graphSize];

        }

        public void InsertDependency(int instruction1Address, int instruction2Address)
        {
            _predecessor[instruction2Address] = _predecessor[instruction1Address] + 1;
        }

        public int GetLongestPathValue()
        {
            //TopologicalSort();
            //_distances[instructionAddress] = 0;
            //UpdateMaxDistanceForAllAdjacents();
            //for (int i = 0; i < _graphSize; i++)
            //{
            //    Console.WriteLine(_distances[i]);
            //}
            int cycles = 0;

            for (int i = 0; i < _graphSize; i++)
            {
                if (cycles < _predecessor[i])
                {
                    cycles = _predecessor[i];
                }
            }
            return cycles;
        }

    }
}