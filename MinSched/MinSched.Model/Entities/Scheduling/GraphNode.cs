﻿using System.Collections.Generic;

namespace MinSched.Model.Entities.Scheduling
{
    public class GraphNode
    {
        private int _key;
        private IEnumerable<GraphNode> _childs;

        public int Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public IEnumerable<GraphNode> Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
    }
}