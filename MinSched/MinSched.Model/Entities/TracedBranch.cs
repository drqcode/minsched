﻿namespace MinSched.Model.Entities
{
    public class TracedBranch
    {
        private int _startAddress;
        private int _jumpAddress;

        public int StartAddress
        {   get
            {
                return _startAddress;
            }

            set
            {
                _startAddress = value;
            }
        }

        public int JumpAddress
        {
            get
            {
                return _jumpAddress;
            }

            set
            {
                _jumpAddress = value;
            }
        }

        public TracedBranch(int start,int jump) {
            StartAddress = start;
            JumpAddress = jump;
        }


    
    }
}
