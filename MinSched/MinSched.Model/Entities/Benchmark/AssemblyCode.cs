using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MinSched.Model.Interface;

namespace MinSched.Model.Entities.Benchmark
{
    public class AssemblyCode
    {
        private readonly IList<Instruction> _codeInstructions;
        private readonly int _exitInstructionAddress;

        public AssemblyCode(string file)
        {
            IInstructionReader reader = new AssemblyCodeFileReader(file);
            _codeInstructions = reader.Read().ToList();
            reader.Dispose();

            _exitInstructionAddress = GetExitInstructionAddress();

        }

        public int LastAddress => _codeInstructions.Count - 1;
        public int ExitAddress => _exitInstructionAddress;



        public IEnumerable<Instruction> GetBasicBlock(int startAddress, int endAddress)
        {
            for (var i = startAddress; i <= endAddress; i++)
            {
                yield return _codeInstructions[i];
            }
        }

        public IEnumerable<Instruction> GetAllInstructions()
        {
            try
            {
                return _codeInstructions;
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException("Assembly source code is not loaded!");
            }
        }

        private int GetExitInstructionAddress()
        {
            foreach (var codeInstruction in _codeInstructions)
            {
                if (!codeInstruction.IsExitInstruction()) continue;
                return _codeInstructions.IndexOf(codeInstruction);
            }
            throw new Exception("TRAP #0 not found!");
        }

        private class AssemblyCodeFileReader : IInstructionReader
        {
            private readonly StreamReader _streamReader;
            public AssemblyCodeFileReader(string filePath)
            {
                _streamReader = new StreamReader(filePath);
            }

            public void Dispose()
            {
                _streamReader.Dispose();
            }

            public IEnumerable<Instruction> Read()
            {
                foreach (var sourceLine in ReadSourceLines())
                {
                    var opcode = sourceLine.Substring(0, sourceLine.IndexOf(' '));
                    var parts = sourceLine.Substring(sourceLine.IndexOf(' ') + 1).Trim().Split(',');
                    //<temporary>
                    if (opcode == "ST" || opcode == "LD")
                    {
                        ModifyPartsForMemoryInstructionWithIndexing(parts, sourceLine, opcode);
                    }
                    //</temporary>
                    var operands = new string[3];

                    for (var i = 0; i < parts.Length; i++)
                    {
                        parts[i] = parts[i].Trim();
                        if (!string.IsNullOrWhiteSpace(parts[i]))
                        {
                            operands[i] = parts[i];
                        }
                    }

                    var instruction = new Instruction(OpcodeMapper.Map(opcode))
                    {
                        Destination = operands[0],
                        Source1 = operands[1],
                        Source2 = operands[2]
                    };
                    yield return instruction;
                }
            }

            private void ModifyPartsForMemoryInstructionWithIndexing(string[] parts, string sourceLine, string opcode)
            {
                if (!sourceLine.Contains("(R")) return;
                if (opcode == "ST")
                {
                    parts[1] = parts[1].Trim();
                    parts[0] = parts[0] + "," + parts[1];
                    parts[1] = parts[2];
                    parts[2] = string.Empty;
                }
                if (opcode != "LD") return;
                parts[2] = parts[2].Trim();
                parts[1] = parts[1] + "," + parts[2];
                parts[2] = string.Empty;
            }

            private IEnumerable<string> ReadSourceLines()
            {
                while (!_streamReader.EndOfStream)
                {
                    var rawLine = _streamReader.ReadLine();
                    rawLine = rawLine?.Trim();
                    if (!IsCodeInstruction(rawLine)) continue;
                    if (ContainsLabel(rawLine))
                    {
                        rawLine = rawLine?.Split(':')[1].Trim();
                    }
                    yield return rawLine;

                }
            }

            private static bool ContainsLabel(string rawString)
            {
                return rawString.Contains(":");
            }
            private static bool IsCodeInstruction(string rawString)
            {
                if (!rawString.Contains(":"))
                    return !rawString.Contains(".") && rawString != string.Empty && !rawString.StartsWith("/");
                if (rawString.Contains(".")) return false;
                var parts = rawString.Split(':');
                parts[0] = parts[0].Trim();
                parts[1] = parts[1].Trim();
                return parts[1] != string.Empty;
            }
        }

        internal static class OpcodeMapper
        {
            public static Opcode Map(string opcode)
            {
                switch (opcode)
                {
                    case "ADD": return Opcode.ADD;
                    case "SUB": return Opcode.SUB;
                    case "MOV": return Opcode.MOV;
                    case "BSR": return Opcode.BSR;
                    case "TRAP": return Opcode.TRAP;
                    case "ST": return Opcode.ST;
                    case "LD": return Opcode.LD;
                    case "MULT": return Opcode.MULT;
                    case "AND": return Opcode.AND;
                    case "DIV": return Opcode.DIV;
                    case "LES": return Opcode.LES;
                    case "BT": return Opcode.BT;
                    case "BRA": return Opcode.BRA;
                    case "GES": return Opcode.GES;
                    case "LTS": return Opcode.LTS;
                    case "GTS": return Opcode.GTS;
                    case "NE": return Opcode.NE;
                    case "EQ": return Opcode.EQ;
                    case "GTE": return Opcode.GTE;
                    case "LTE": return Opcode.LTE;
                    case "GT": return Opcode.GT;
                    case "LT": return Opcode.LT;
                    case "NOP": return Opcode.NOP;
                    case "LSR":
                    return Opcode.LSR;
                    case "ASR":
                        return Opcode.ASR;
                    case "ASL":
                        return Opcode.ASL;
                    case "LSL":
                        return Opcode.LSL;
                    default:
                    {
                        throw new Exception("Opcode " + opcode + " not understand!");
                    }
                }
            }
        }
    }
}