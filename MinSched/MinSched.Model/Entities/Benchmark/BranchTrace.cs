using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MinSched.Model.Interface;

namespace MinSched.Model.Entities.Benchmark
{
    public class BranchTrace
    {
        private readonly IList<TracedBranch> _branches;

        public BranchTrace(string file)
        {
            var reader = new TracedBranchFileReader(file);
            _branches = reader.Read().ToList();
            reader.Dispose();
        }

        public IList<TracedBranch> GetNearTargetBranches(int assemblyCodeLastAddress)
        {
            return _branches.Where(tracedBranch => tracedBranch.JumpAddress <= assemblyCodeLastAddress
                                                && tracedBranch.StartAddress <= assemblyCodeLastAddress).ToList();
        }

        private class TracedBranchFileReader : ITracedBranchReader
        {
            private readonly StreamReader _streamReader;


            public TracedBranchFileReader(string filePath)
            {
                _streamReader = new StreamReader(filePath);
            }

            public void Dispose()
            {
                _streamReader.Close();
            }

            public IList<TracedBranch> Read()
            {
                IList<TracedBranch> newList = new List<TracedBranch>();
                while (!_streamReader.EndOfStream)
                {
                    var rawLine = _streamReader.ReadLine();
                    var parts = rawLine?.Split(' ').Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
                    for (var i = 0; i <= parts?.Count - 3; i = i + 3)
                    {
                        if (parts[i] != "B") continue;
                        newList.Add(new TracedBranch(Convert.ToInt32(parts[i + 1]), Convert.ToInt32(parts[i + 2])));
                    }
                }
                return newList;
            }

        }
    }
}