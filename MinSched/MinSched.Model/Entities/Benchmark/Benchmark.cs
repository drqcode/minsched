using System.IO;

namespace MinSched.Model.Entities.Benchmark
{
    public class Benchmark
    {
        private string _name;
        private AssemblyCode _assemblyCode;
        private BranchTrace _branchTrace;

 
        public AssemblyCode AssemblyCode
        {
            get { return _assemblyCode; }
        }

        public BranchTrace BranchTrace
        {
            get { return _branchTrace; }
        }


        public string Name
        {
            get { return _name; }
        }

        

        public Benchmark(string assemblyFile, string branchTraceFile)
        {
            _name = Path.GetFileNameWithoutExtension(assemblyFile);

            _assemblyCode = new AssemblyCode(assemblyFile);
            _branchTrace=new BranchTrace(branchTraceFile);
        }
    }
}