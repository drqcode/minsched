﻿
using System;
using System.Text;

namespace MinSched.Model.Entities
{
    public class Instruction
    {
        private Opcode _opcode;
        private string _destination;
        private string _source1;
        private string _source2;

        public Instruction(Opcode opcode)
        {
            _opcode = opcode;
            _destination = string.Empty;
        }

        private Instruction(Instruction instruction)
        {
            _opcode = instruction.Opcode;
            _destination = instruction.Destination;
            _source1 = instruction.Source1;
            _source2 = instruction.Source2;
        }

        public Opcode Opcode
        {
            get { return _opcode; }
            set { _opcode = value; }
        }

        public bool IsImmediateType()
        {
            var isImmediate = false;
            if (_source1 != null)
            {
                if (!_source1.Contains("_"))
                {
                    isImmediate = _source1.StartsWith("#");

                }
            }
            if (_source2 != null)
            {
                if (!_source2.Contains("_"))
                {
                    isImmediate = _source2.StartsWith("#");

                }
            }
            return isImmediate;
        }

        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }

        public string Source1
        {
            get { return _source1; }
            set { _source1 = value; }
        }

        public string Source2
        {
            get { return _source2; }
            set { _source2 = value; }
        }

        public bool IsMemoryInstruction()
        {
            return _opcode == Opcode.LD || _opcode == Opcode.LD;
        }

        public bool IsRelationalType()
        {
            return
                _opcode == Opcode.GT ||
                _opcode == Opcode.GTE ||
                _opcode == Opcode.LT ||
                _opcode == Opcode.LTE ||
                _opcode == Opcode.NE ||
                _opcode == Opcode.EQ;
        }

        public string BaseRegisterForMemoryInstruction()
        {
            var tmp = string.Empty;
            if (_opcode == Opcode.LD)
            {
                if (_source1.Contains(","))
                {
                    tmp = _source1.Substring(1, _source1.IndexOf(',') - 1).Trim();
                }
                else if (_source1.Contains("(R"))
                {
                    tmp = _source1.Substring(1, _source1.IndexOf(')') - 1).Trim();
                }
            }
            if (_opcode != Opcode.ST) return tmp;
            if (_destination.Contains(","))
            {
                tmp = _destination.Substring(1, _destination.IndexOf(',') - 1).Trim();
            }
            else if (_destination.Contains("(R"))
            {
                tmp = _destination.Substring(1, _destination.IndexOf(')') - 1).Trim();
            }
            return tmp;
        }

        public string IndexRegisterForMemoryInstruction()
        {
            var tmp = string.Empty;
            if (_opcode == Opcode.LD && _source1.Contains(","))
            {
                tmp = _source1.Substring(_source1.IndexOf(',') + 1, _source1.IndexOf(')') - _source1.IndexOf(',')).Trim();
            }
            if (_opcode == Opcode.ST && _destination.Contains(","))
            {
                tmp = _destination.Substring(_destination.IndexOf(',') + 1, _destination.IndexOf(')') - _source1.IndexOf(',')).Trim();
            }
            return tmp;
        }

        public bool IsBranchType()
        {
            return _opcode == Opcode.BRA || _opcode == Opcode.BSR || _opcode == Opcode.BT || _opcode == Opcode.TRAP || _opcode == Opcode.MOV && _destination == "PC";
        }

        public bool IsExitInstruction()
        {
            return _opcode == Opcode.TRAP && _destination == "#0";
        }

        public int GetImmediateValue()
        {
            //delete '#' start char of immediat value
            if (_source2 != null)
            {
                if (_source2.StartsWith("#"))
                {
                    return Convert.ToInt32(_source2.Substring(1));
                }
            }
            if (_source1 != null)
            {
                if (_source1.StartsWith("#"))
                {
                    return Convert.ToInt32(_source1.Substring(1));
                }
            }
            throw new FormatException();
        }

        public bool IsUnary()
        {
            return (_source1 == null && _source2 == null);
        }

        public override string ToString()
        {
            var temp = new StringBuilder(_opcode.ToString());
            temp.Append(_destination != string.Empty ? " " + _destination : string.Empty);
            temp.Append(_source1 != null ? ", " + _source1 : null);
            temp.Append(_source2 != null ? ", " + _source2 : null);
            return temp.ToString();
        }

        public Instruction Clone()
        {
            return new Instruction(this);
        }
    }
}