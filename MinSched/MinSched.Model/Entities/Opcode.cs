﻿namespace MinSched.Model.Entities
{
    public enum Opcode
    {
        ADD, SUB , MOV, GTE, LTE, GT, LT, EQ, NE,
        BSR,
        TRAP,
        ST,
        LD,
        MULT,
        AND,
        DIV,
        LES,
        BT,
        BRA,
        GES,
        LTS,
        GTS,
        NOP,
        LSR,
        ASR,
        ASL,
        LSL
    }
}