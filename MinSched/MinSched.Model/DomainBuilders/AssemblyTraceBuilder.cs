﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MinSched.Model.Entities;

namespace MinSched.Model.DomainBuilders
{
    public class AssemblyTraceBuilder
    {
        private IList<Instruction> _instructions;

        public AssemblyTraceBuilder()
        {
            _instructions = new List<Instruction>();
        }

        public void AppendInstruction(Instruction instruction)
        {
            _instructions.Add(instruction);
        }

        public void AppendBlock(IEnumerable<Instruction> block)
        {
            foreach (var instruction in block)
            {
                _instructions.Add(instruction);
            }
        }

        public IEnumerable<Instruction> GetInstructions()
        {
            return _instructions;
        }
    }
}