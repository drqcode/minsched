﻿using System;
using System.IO;
using System.Reflection;
using MinSched.Infrastructure.Properties;

namespace MinSched.Infrastructure
{
    public class UserFileManager
    {
        public static UserFileManager Instance = new UserFileManager();

        private readonly string _userDir;
        private readonly string _assemblyTracesDir;
        private readonly string _scheduledTracesDir;

        private UserFileManager()
        {
            _userDir =
                Path.GetDirectoryName(
                    Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)) + "\\" +
                Resources.UserFileDir;
            _assemblyTracesDir = _userDir + "\\Assembly traces\\";
            _scheduledTracesDir = _userDir + "\\Scheduled traces\\";
            if (!Directory.Exists(_userDir))
            {
                Directory.CreateDirectory(_userDir);
            }
            if (!Directory.Exists(_assemblyTracesDir))
            {
                Directory.CreateDirectory(_assemblyTracesDir);
            }
            if (!Directory.Exists(_scheduledTracesDir))
            {
                Directory.CreateDirectory(_scheduledTracesDir);
            }
        }


        public string GetFilePath(string fileName)
        {
            var fileExtension = Path.GetExtension(fileName);
            switch (fileExtension)
            {
                case ".btrc":
                    return _assemblyTracesDir  + fileName;
                case ".trc":
                    return _userDir  + fileName;
                case ".strc":
                    return _scheduledTracesDir + fileName;
                default: throw new FileNotFoundException("Unknown file format!");
            }

        }

    }
}