﻿using System;
using System.Windows.Forms;
using MinSchedUI.Forms;

namespace MinSchedUI
{
    public class MinSchedAppContext : ApplicationContext
    {
        private Form _mainForm;
        private Timer _spashTimer = new Timer();

        public MinSchedAppContext(Form mainForm, Form splashForm):base(splashForm)
        {
            _mainForm = mainForm;
            _spashTimer.Tick += SplashTimeUp;
            _spashTimer.Interval = 3000;
            _spashTimer.Enabled = true;
        }

        private void SplashTimeUp(object sender, EventArgs e)
        {
            _spashTimer.Enabled = false;
            _spashTimer.Dispose();

            MainForm.Close();
        }

        protected override void OnMainFormClosed(object sender, EventArgs e)
        {
            if (sender is SplashForm)
            {
                MainForm = _mainForm;
                MainForm.Show();
            }
            else
            {
                base.OnMainFormClosed(sender, e);
            }
        }
    }
}