﻿using System;
using System.Windows.Forms;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Entities.Scheduling;
using MinSched.Tracer;
using MinSched.Tracer.TracingImpl;

namespace MinSchedUI.Forms
{
    public partial class IntroForm : Form
    {
        public IntroForm()
        {
            InitializeComponent();
        }

        private void IntroForm_Load(object sender, System.EventArgs e)
        {
            const string sourceCode =
    @"C:\Users\constantin.capatina\Desktop\Simulator HSA-20161211T150856Z\Simulator HSA\FBUBBLE.INS";
            const string branchTrace =
                @"C:\Users\constantin.capatina\Desktop\Benchmarks&Traces-20161211T150825Z\Benchmarks_Traces\FBUBBLE\FBUBBLE.TRC";
            var assembly = new AssemblyCode(sourceCode);
            var trace = new BranchTrace(branchTrace);

            //var address = 0;
            //foreach (var instruction in assembly.GetAllInstructions())
            //{
            //    Console.WriteLine(address+": "+instruction);
            //    address++;
            //}

            //foreach (var nearTargetBranch in trace.GetNearTargetBranches(assembly.LastAddress))
            //{
            //    Console.WriteLine(nearTargetBranch.StartAddress+" "+nearTargetBranch.JumpAddress);
            //}

            //first block; os target the program
            var benchmark = new Benchmark(sourceCode, branchTrace);
            TraceGenerator tracer = new LargeBasicBlockGenerator(benchmark);
            tracer.TraceGenerated += delegate (AssemblyTrace assemblyTrace)
            {
                var instructionsCount = 0;
                foreach (var assemblyTraceInstruction in assemblyTrace.AssemblyInstructions)
                {
                    Console.WriteLine(assemblyTraceInstruction);
                    instructionsCount++;
                }
                Console.WriteLine(instructionsCount);
            };
            tracer.GenerateTrace();
        }
    }
}
