﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MinSched.Model.Entities.Scheduling;

namespace MinSched.Simulator
{
    public delegate void SimulateEventHandler(double ipc, string name,int windowSize); 
    public class CpiSimulator
    {
        public event SimulateEventHandler SimulatedDone;


        public void Simulate(ScheduledAssembyTrace trace)
        {
            var nrOfInstr = trace.NumberOfInstructions;
            var cycles = trace.DependencyGraphs.Sum(traceDependencyGraph => traceDependencyGraph.GetLongestPathValue() + 1);
            var ipc = (double)nrOfInstr/cycles;
            ipc = Math.Round(ipc, 2);
            SimulatedDone?.Invoke(ipc,trace.Name,trace.WindowSize);
        }
    }
}
