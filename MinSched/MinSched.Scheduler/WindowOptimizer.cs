﻿using System;
using System.Diagnostics;
using System.Threading;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Scheduling;
using MinSched.Scheduler.DependencyAnalysis;

namespace MinSched.Scheduler
{
    public class WindowOptimizer
    {
        private readonly ScheduledAssembyTrace _trace;
        private ManualResetEvent _doneEvent;
        private int _threadCount;

        public WindowOptimizer(ScheduledAssembyTrace trace, ManualResetEvent doneEvent)
        {
            _trace = trace;
            _doneEvent = doneEvent;
            _threadCount = 0;
        }

        public void OptimizeWindow(int windowAddress)
        {
            //var window = _trace.GetWindow(windowAddress);
            //var graph = new TrueDependencyGraph(_trace.WindowSize);

            //for (var i = 0; i < window.Count - 1; i++)
            //{
            //    for (var j = i + 1; j < window.Count; j++)
            //    {
            //        var dependencyType = DependencyAnalizer.GetDependency(window[i], window[j]);

            //        if (TechniquePool.Instance.IsTechniqueChecked(dependencyType))
            //        {
            //            TechniquePool.Instance.GetTechnique(dependencyType).Apply(window[i], window[j]);
            //            continue;
            //        }
            //        if (dependencyType == DependencyResolverType.NoDependency)
            //        {
            //            continue;
            //        }
            //        graph.InsertDependency(i, j);
            //    }
            //}
            //_trace.DependencyGraphs.Add(graph);

            Interlocked.Increment(ref _threadCount);
            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    var window = _trace.GetWindow(windowAddress);
                    var graph = new TrueDependencyGraph(_trace.WindowSize);

                    for (var i = 0; i < window.Count - 1; i++)
                    {
                        for (var j = i + 1; j < window.Count; j++)
                        {
                            var dependencyType = DependencyAnalizer.GetDependency(window[i], window[j]);

                            if (TechniquePool.Instance.IsTechniqueChecked(dependencyType))
                            {
                                TechniquePool.Instance.GetTechnique(dependencyType).Apply(window[i], window[j]);
                                continue;
                            }
                            if (dependencyType == DependencyResolverType.NoDependency)
                            {
                                continue;
                            }
                            graph.InsertDependency(i, j);
                        }
                    }
                    _trace.DependencyGraphs.Add(graph);
                }
                finally
                {
                    //Signal the caller thread
                    if (Interlocked.Decrement(ref _threadCount) == 0)
                    {
                        _doneEvent.Set();
                    }
                }
            });
        }
    }
}