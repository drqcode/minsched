﻿using System.Diagnostics;
using System.Threading;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Scheduling;

namespace MinSched.Scheduler
{
    public class CodeSchedulerImpl : CodeScheduler
    {
        private int _numberOfWindows;
        private ManualResetEvent _doneEvent;

        public CodeSchedulerImpl(AssemblyTrace trace, int windowInstructionSize) : base(trace)
        {
            //set window size (its throwing exception if is not in range)
            Trace.WindowSize = windowInstructionSize;

            _numberOfWindows = Trace.NumberOfInstructions % Trace.WindowSize == 0
                ? Trace.NumberOfInstructions / Trace.WindowSize
                : Trace.NumberOfInstructions / Trace.WindowSize + 1;

            _doneEvent = new ManualResetEvent(false);
        }

        public override void Schedule()
        {
            SchedulerThread = new Thread(ScheduleCode);
            SchedulerThread.Start();
        }

        private void ScheduleCode()
        {
            var windowOptimizer = new WindowOptimizer(Trace ,_doneEvent);
            for (var windowAddress = 0; windowAddress < Trace.NumberOfInstructions; windowAddress = windowAddress + Trace.WindowSize)
            {
                windowOptimizer.OptimizeWindow(windowAddress);
            }
            _doneEvent.WaitOne();
            OnScheduled(Trace);
        }
    }
}