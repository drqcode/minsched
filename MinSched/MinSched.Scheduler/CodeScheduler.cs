﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Scheduling;

namespace MinSched.Scheduler
{
    public delegate void TraceScheduledEventHandler(ScheduledAssembyTrace scheduledTrace);
    public abstract class CodeScheduler
    {
        protected Thread SchedulerThread;
        protected ScheduledAssembyTrace Trace;

        public event TraceScheduledEventHandler Scheduled;

        protected CodeScheduler(AssemblyTrace trace)
        {
            Trace = new ScheduledAssembyTrace(trace);
        }

        public abstract void Schedule();

        protected virtual void OnScheduled(ScheduledAssembyTrace trace)
        {
            Scheduled?.Invoke(trace);
        }
    }
}
