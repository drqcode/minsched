﻿using System;
using System.Collections.Generic;
using System.Text;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Scheduling;
using MinSched.Scheduler.ScheduleTechniques;

namespace MinSched.Scheduler
{
    public delegate void TechniquePoolChangedEventHandler(ICollection<DependencyResolverType> checkedTechniques);

    public class TechniquePool
    {
        public static TechniquePool Instance = new TechniquePool();

        private IDictionary<DependencyResolverType, ITechnique> _techniques;

        private TechniquePool()
        {
            _techniques = new Dictionary<DependencyResolverType, ITechnique>();
        }

        public event TechniquePoolChangedEventHandler Changed;

        public void CheckTechnique(DependencyResolverType technique)
        {
            switch (technique)
            {
                case DependencyResolverType.AntiAliasMemory:
                {
                    _techniques.Add(technique, new AntiAliasMemoryTechnique(0));
                    Changed?.Invoke(_techniques.Keys);
                    return;
                }
                case DependencyResolverType.ImmediateMerging:
                {
                    _techniques.Add(technique, new ImmediateMergingTechnique(0));
                    Changed?.Invoke(_techniques.Keys);
                    return;
                }
                case DependencyResolverType.MovMerging:
                {
                    _techniques.Add(technique, new MoveMergingTechnique(0));
                    Changed?.Invoke(_techniques.Keys);
                    return;
                }
                case DependencyResolverType.MovReabsorption:
                {
                    _techniques.Add(technique, new MoveReabsorptionTechnique(0));
                    Changed?.Invoke(_techniques.Keys);
                    return;
                }
                case DependencyResolverType.NoDependency: return;
                case DependencyResolverType.TrueDependency: return;
                default: return;
            }
        }

        public string GetConfiguration()
        {
            var confBuilder = new StringBuilder();
            if (_techniques.Count==0)
            {
                confBuilder.Append("NoConfig");
                return confBuilder.ToString();
            }
            foreach (var dependencyResolverType in _techniques.Keys)
            {
                var dependency = dependencyResolverType.ToString();
                confBuilder.Append(dependency.Substring(0,4)).Append("/");
            }
            return confBuilder.ToString();

        }

        public void UncheckTechnique(DependencyResolverType technique)
        {
            _techniques.Remove(technique);
            Changed?.Invoke(_techniques.Keys);
        }

        public ITechnique GetTechnique(DependencyResolverType technique)
        {
            return _techniques[technique];
        }

        public bool IsTechniqueChecked(DependencyResolverType technique)
        {
            return _techniques.Keys.Contains(technique);
        }
    }
}