﻿using System;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Scheduling;

namespace MinSched.Scheduler.DependencyAnalysis
{
    public static class DependencyAnalizer
    {
        public static DependencyResolverType GetDependency(Instruction instruction1, Instruction instruction2)
        {
            var dependency = DependencyResolverType.NoDependency;
            //check for AntiAlias
            if (instruction1.Opcode == Opcode.ST && instruction2.Opcode == Opcode.LD && instruction1.Destination == instruction2.Source1)
            {
                dependency = DependencyResolverType.AntiAliasMemory;
                return dependency;
            }
            //If instruction 2 doesn't have sources there is no RAW dependency
            if (instruction2.IsUnary() && instruction2.IsImmediateType())
            {
                dependency = DependencyResolverType.NoDependency;
            }
            //check for RAW
            if (instruction2.IsUnary() && instruction1.Destination == instruction2.Destination)
            {
                dependency = DependencyResolverType.TrueDependency;
            }
            if (instruction2.IsMemoryInstruction())
            {
                if (instruction1.Destination == instruction2.BaseRegisterForMemoryInstruction() ||
                    instruction1.Destination == instruction2.IndexRegisterForMemoryInstruction())
                {
                    dependency = DependencyResolverType.TrueDependency;
                }
            }
            if (instruction1.Destination == instruction2.Source1 ||
                instruction1.Destination == instruction2.Source2)
            {
                dependency = DependencyResolverType.TrueDependency;
            }

            //if exists RAW dependency then continue checking the techniques
            if (dependency != DependencyResolverType.TrueDependency)
            {
                return dependency;
            }
            //Immediate Merging check
            if ((instruction1.Opcode == Opcode.ADD || instruction1.Opcode == Opcode.SUB) &&
                (instruction2.Opcode == Opcode.ADD || instruction2.Opcode == Opcode.SUB) &&
                (instruction1.Source1!=instruction2.Source1) &&
                (instruction1.IsImmediateType() && instruction2.IsImmediateType()))
            {
                dependency = DependencyResolverType.ImmediateMerging;
                return dependency;
            }
            //MOV Merging check
            if (instruction1.Opcode == Opcode.MOV)
            {
                if ((instruction2.Opcode == Opcode.ADD && !instruction1.IsImmediateType() && !instruction2.IsImmediateType()) ||
                    (instruction1.IsImmediateType() && (instruction2.Opcode == Opcode.ADD || instruction2.Opcode==Opcode.SUB) && instruction2.IsImmediateType()) ||
                    (instruction1.IsImmediateType() && instruction2.IsRelationalType()) ||
                    (!instruction1.IsImmediateType() && instruction2.Opcode == Opcode.BT))
                {
                    dependency = DependencyResolverType.MovMerging;
                    return dependency;
                }
                if (instruction1.IsImmediateType() && instruction2.Opcode == Opcode.ST)
                {
                    if (instruction1.GetImmediateValue() == 0)
                    {
                        dependency = DependencyResolverType.MovMerging;
                        return dependency;
                    }
                }
            }

            if (instruction1.Opcode == Opcode.EQ && instruction1.Source1 == "R0" && instruction1.Source2 == "R0" && instruction2.Opcode == Opcode.BT)
            {
                dependency = DependencyResolverType.MovMerging;
                return dependency;
            }

            //MOV Reabsorption check
            if (!instruction1.IsImmediateType() && !instruction2.IsImmediateType() &&
                instruction2.Opcode == Opcode.MOV && (instruction1.Opcode == Opcode.ADD || instruction1.Opcode == Opcode.SUB))
            {
                dependency = DependencyResolverType.MovReabsorption;
                return dependency;
            }

            return dependency;
        }
    }
}