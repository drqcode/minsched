﻿using MinSched.Model.Entities;
using System.Xml.Linq;

namespace MinSched.Scheduler
{
    public interface ITechnique
    {
        void Apply(Instruction instruction1, Instruction instruction2);

    }
}