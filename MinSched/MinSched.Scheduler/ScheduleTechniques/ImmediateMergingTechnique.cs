using MinSched.Model.Entities;

namespace MinSched.Scheduler.ScheduleTechniques
{
    public class ImmediateMergingTechnique : ITechnique
    {
        public ImmediateMergingTechnique(int priority)
        {
            Priority = priority;
        }

        public int Priority { get; set; }

        public void Apply(Instruction instruction1, Instruction instruction2)
        {
            var x = instruction1.GetImmediateValue();
            var y = instruction2.GetImmediateValue();

            instruction2.Source1 = instruction1.Source1;
            if (instruction1.Opcode == instruction2.Opcode)
            {
                instruction2.Source2 = "#" + (x + y);
            }
            else if (instruction1.Opcode == Opcode.ADD)
            {
                instruction2.Source2 = "#" + (x - y);
            }
            else
            {
                instruction2.Source2 = "#" + (y - x);
            }
        }
    }
}