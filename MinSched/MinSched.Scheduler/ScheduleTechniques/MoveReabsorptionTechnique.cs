using MinSched.Model.Entities;

namespace MinSched.Scheduler.ScheduleTechniques
{
    public class MoveReabsorptionTechnique : ITechnique
    {
        public MoveReabsorptionTechnique(int priority)
        {
            Priority = priority;
        }

        public int Priority { get; set; }
        
        public void Apply(Instruction instruction1, Instruction instruction2)
        {
            instruction2.Opcode = instruction1.Opcode;
            instruction2.Source1 = instruction1.Source1;
            instruction2.Source2 = instruction1.Source1;
        }
    }
}