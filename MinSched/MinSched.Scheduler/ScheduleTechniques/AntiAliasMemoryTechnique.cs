using MinSched.Model.Entities;

namespace MinSched.Scheduler.ScheduleTechniques
{
    public class AntiAliasMemoryTechnique : ITechnique
    {
        public AntiAliasMemoryTechnique(int priority)
        {
            Priority = priority;
        }

        public int Priority { get; set; }


        public void Apply(Instruction instruction1, Instruction instruction2)
        {
            if (instruction1.Source1 == instruction2.Source2)
            {
                instruction2.Opcode = instruction1.Opcode;
                string dest = instruction2.Destination;
                instruction2.Destination = instruction1.Destination;
                instruction2.Source1 = instruction1.Source1;

                instruction1.Opcode = Opcode.MOV;
                instruction1.Destination = dest;
            }
        }
    }
}