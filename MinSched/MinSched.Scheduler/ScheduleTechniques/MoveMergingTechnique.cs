using System;
using MinSched.Model.Entities;

namespace MinSched.Scheduler.ScheduleTechniques
{
    public class MoveMergingTechnique : ITechnique
    {

        public MoveMergingTechnique(int priority)
        {
            Priority = priority;
        }

        public int Priority { get; set; }

        public void Apply(Instruction instruction1, Instruction instruction2)
        {
            //branch with dependency
            if (instruction1.Opcode == Opcode.MOV && instruction2.Opcode == Opcode.BT)
            {
                instruction2.Destination = instruction1.Source1;
                return;
            }
            //branch allways constant
            if (instruction1.Opcode == Opcode.EQ && instruction2.Opcode == Opcode.BT)
            {
                if (instruction1.Source2 == instruction1.Source1)
                {
                    instruction1.Opcode = Opcode.NOP;
                    instruction1.Destination = instruction1.Source1 = instruction1.Source2 = "NOP";
                    instruction2.Opcode = Opcode.BRA;
                    instruction2.Destination = instruction2.Source1;
                    instruction2.Source1 = "NOP";
                    return;
                }
                else
                {
                    instruction2.Opcode = Opcode.NOP;
                    instruction2.Destination = instruction2.Source1 = "NOP";
                }
            }


            if (instruction1.IsImmediateType())
            {
                var x = instruction1.GetImmediateValue();

                if (x == 0 && instruction2.Opcode == Opcode.ST)
                {
                    instruction2.Source1 = "R0";
                    return;
                }
            }
            if (instruction2.Opcode == Opcode.ADD || instruction2.Opcode == Opcode.SUB)
            {
                //de adunare cu sau fara immediate
                if (instruction2.IsImmediateType() && instruction1.IsImmediateType())
                {
                    var x = instruction1.GetImmediateValue();
                    var y = instruction2.GetImmediateValue();
                    instruction2.Opcode = Opcode.MOV;
                    if (instruction2.Opcode == Opcode.SUB)
                    {
                        instruction2.Source1 = "#" + (x - y);
                    }
                    else
                    {
                        instruction2.Source1 = "#" + (x + y);
                    }
                    instruction2.Source2 = null;
                }
                else if (!instruction2.IsImmediateType() && !instruction1.IsImmediateType())
                {
                    instruction2.Source1 = instruction1.Source1;
                }
            }
            else
            {
                //relationala
                switch (instruction2.Opcode)
                {
                    case Opcode.GT:
                    instruction2.Opcode = Opcode.LTE;
                    break;
                    case Opcode.LT:
                    instruction2.Opcode = Opcode.GTE;
                    break;
                    case Opcode.GTE:
                    instruction2.Opcode = Opcode.LT;
                    break;
                    case Opcode.LTE:
                    instruction2.Opcode = Opcode.GT;
                    break;
                    case Opcode.EQ:
                    instruction2.Opcode = Opcode.NE;
                    break;
                    case Opcode.NE:
                    instruction2.Opcode = Opcode.EQ;
                    break;
                    default:
                    throw new ArgumentOutOfRangeException();
                }
                instruction2.Source1 = instruction2.Source2;
                instruction2.Source2 = "#" + instruction1.GetImmediateValue();
            }
        }
    }
}