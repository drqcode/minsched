﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MinSched.Infrastructure;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Entities.Scheduling;
using MinSched.Scheduler;
using MinSched.Tracer;
using MinSched.Tracer.TracingImpl;

namespace MinSched.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private ManualResetEvent _doneEvent = new ManualResetEvent(false);
        private AssemblyTrace _testTrace;

        [TestMethod]
        public void TestMethod1()
        {
            //TraceGenerator tgi = new BufferTraceGenerator("FBUBBLE.INS", "FBUBBLE.TRC");

            //tgi.GenerateTrace();
        }

        [TestMethod]
        public void SaveTraceToFile_Test()
        {
            const string sourceCode =
                @"C:\Users\constantin.capatina\Desktop\Simulator HSA-20161211T150856Z\Simulator HSA\FBUBBLE.INS";
            const string branchTrace =
                @"C:\Users\constantin.capatina\Desktop\Benchmarks&Traces-20161211T150825Z\Benchmarks_Traces\FBUBBLE\FBUBBLE.TRC";
            var assembly = new AssemblyCode(sourceCode);
            var trace = new BranchTrace(branchTrace);

            var benchmark = new Benchmark(sourceCode, branchTrace);
            TraceGenerator tracer = new LargeBasicBlockGenerator(benchmark);
            tracer.TraceGenerated += delegate (AssemblyTrace assemblyTrace)
            {
                assemblyTrace.SaveTrace();
                _doneEvent.Set();
            };
            tracer.GenerateTrace();
            _doneEvent.WaitOne();
        }

        [TestMethod]
        public void InstructionParser_Test()
        {
            const string insTest = @"C:\Users\constantin.capatina\Desktop\testAsm.ins";
            var assembly1 = new AssemblyCode(insTest);
            foreach (var ins in assembly1.GetAllInstructions())
            {
                Console.WriteLine(ins);
            }
        }

        [TestMethod]
        public void MaxDistanceInGraph_Test()
        {
            TrueDependencyGraph graph = new TrueDependencyGraph(10);
            graph.InsertDependency(0, 1);
            graph.InsertDependency(0, 2);
            graph.InsertDependency(0, 3);
            graph.InsertDependency(0, 5);
            graph.InsertDependency(5, 8);
            graph.InsertDependency(4, 9);
            graph.InsertDependency(6, 7);

            Console.WriteLine(graph.GetLongestPathValue());
        }

        [TestMethod]
        public void Tracer_LargeBasicBlock_Test()
        {
            _doneEvent = new ManualResetEvent(false);

            TechniquePool.Instance.CheckTechnique(DependencyResolverType.AntiAliasMemory);
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.ImmediateMerging);
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.MovMerging);
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.MovReabsorption);

            const string sourceCode =
                @"C:\Users\constantin.capatina\Desktop\Simulator HSA-20161211T150856Z\Simulator HSA\FBUBBLE.INS";
            const string branchTrace =
                @"C:\Users\constantin.capatina\Desktop\Benchmarks&Traces-20161211T150825Z\Benchmarks_Traces\FBUBBLE\FBUBBLE.TRC";
            var assembly = new AssemblyCode(sourceCode);
            var trace = new BranchTrace(branchTrace);

            //var address = 0;
            //foreach (var instruction in assembly.GetAllInstructions())
            //{
            //    Console.WriteLine(address+": "+instruction);
            //    address++;
            //}

            //foreach (var nearTargetBranch in trace.GetNearTargetBranches(assembly.LastAddress))
            //{
            //    Console.WriteLine(nearTargetBranch.StartAddress+" "+nearTargetBranch.JumpAddress);
            //}

            //first block; os target the program
            var benchmark = new Benchmark(sourceCode, branchTrace);
            TraceGenerator tracer = new LargeBasicBlockGenerator(benchmark);
            tracer.TraceGenerated += TracerOnTraceGenerated;
            tracer.GenerateTrace();
            _doneEvent.WaitOne();

            CodeScheduler scheduler = new CodeSchedulerImpl(_testTrace, 100);
            scheduler.Scheduled += delegate (ScheduledAssembyTrace scheduledTrace)
            {
                Console.WriteLine(scheduledTrace.Name);
                Console.WriteLine(scheduledTrace.WindowSize);
                Console.WriteLine(scheduledTrace.NumberOfInstructions);
                scheduledTrace.SaveTrace();

                var cycles = 0;
                foreach (var scheduledTraceDependencyGraph in scheduledTrace.DependencyGraphs)
                {
                    cycles += scheduledTraceDependencyGraph.GetLongestPathValue() + 1;
                }

                Console.WriteLine("Cycles = "+cycles);
                _doneEvent.Set();
            };
            _doneEvent.Reset();
            scheduler.Schedule();
            _doneEvent.WaitOne();

        }

        private void TracerOnTraceGenerated(AssemblyTrace trace)
        {
            _testTrace = trace;
            _testTrace.SaveTrace();



            _doneEvent.Set();
        }

    }
}

