﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MinSchedApp
{
    public partial class ChartView : Form
    {
        private ApplicationInstance _applicationInstance;
        public ChartView(ApplicationInstance ai, string resultName)
        {
            InitializeComponent();
            _applicationInstance = ai;
            Chart ceva = CreateChart(resultName);
            Controls.Add(ceva);
        }
        

        public Chart CreateChart(string title)
        {
            chart.Series.Clear();
            chart.Text = title;
            Series series = new Series();
            series.Name = title;
            series.ChartType = SeriesChartType.Column;
            series.Color = Color.Green;
            series.XValueType = ChartValueType.String;
            series.YValueType=ChartValueType.Double;
            chart.DataSource = _applicationInstance.Results[title];
            series.Points.DataBindXY(_applicationInstance.Results[title].Select(x => x.Configuration).ToList(), _applicationInstance.Results[title].Select(y => y.CPI).ToList());
            series.IsValueShownAsLabel = true;
            chart.Series.Add(series);
            chart.Enabled = true;
            chart.Dock = DockStyle.Fill;
            chart.Update();
            
            return chart;


        }


    }
}
