﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;

namespace MinSchedApp
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class OpenFiles : Window
    {
        private ApplicationInstance _applicationInstance;
        private OpenFileDialog _openFileDialog1;
        private OpenFileDialog _openFileDialog2;


        public OpenFiles(ApplicationInstance ai)
        {
            _applicationInstance = ai;
            InitializeComponent();
        }

        private void insBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            _openFileDialog1 = new OpenFileDialog {Filter = "INS Files (*.ins)|*.ins|All files (*.*)|*.*"};


            switch (_openFileDialog1.ShowDialog())
            {
                case true:
                    insTextBox.Text = System.IO.Path.GetFileName(_openFileDialog1.FileName);
                    if (insTextBox.Text != "")
                        finishBtn.IsEnabled = true;
                    break;
                case false:
                    trcTextBox.Text = "";
                    break;
            }
        }

        private void trcBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            _openFileDialog2 = new OpenFileDialog {Filter = "TRC Files (*.trc)|*.trc|All files (*.*)|*.*"};


            switch (_openFileDialog2.ShowDialog())
            {
                case true:
                    trcTextBox.Text = System.IO.Path.GetFileName( _openFileDialog2.FileName);
                    if (insTextBox.Text != "")
                        finishBtn.IsEnabled = true;
                    break;
                case false:
                    trcTextBox.Text = "";
                    break;
            }
            
        }

        private void finishBtn_Click(object sender, RoutedEventArgs e)
        {
            _applicationInstance._benchmarks.Add(new Benchmark(_openFileDialog1.FileName, _openFileDialog2.FileName));
            this.Close();
        }
    }
}
