﻿using MinSched.Model.Entities;
using MinSched.Tracer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Entities.Scheduling;
using MinSched.Scheduler;
using MinSched.Tracer.TracingImpl;
using MinSched.Simulator;

namespace MinSchedApp
{
    public class ApplicationInstance
    {
        private MinSchedConsole _console;

        public IList<Benchmark> _benchmarks;
        public IList<AssemblyTrace> _assemblyTraces;
        private IList<TraceGenerator> _traceGenerators;
        private IList<CodeScheduler> _schedulers;
        public IList<ScheduledAssembyTrace> _scheduledTraces;
        public event EventHandler TraceGenerated;
        public event EventHandler SchedulingDone;
        public event EventHandler SimulateDone;
        public Dictionary<string, IList<ChartResultViewModel>> Results;
        private CpiSimulator _simulator;


        public ApplicationInstance(MinSchedConsole console)
        {
            _console = console;
            _scheduledTraces = new List<ScheduledAssembyTrace>();
            _traceGenerators = new List<TraceGenerator>();
            _benchmarks = new List<Benchmark>();
            _assemblyTraces = new List<AssemblyTrace>();
            _simulator = new CpiSimulator();
            Results=new Dictionary<string, IList<ChartResultViewModel>>();
            _simulator.SimulatedDone+=SimulatorOnSimulatedDone;
        }

        private void SimulatorOnSimulatedDone(double ipc, string name, int windowSize)
        {
            var result = new ChartResultViewModel();
            result.CPI = ipc;
            result.Configuration = TechniquePool.Instance.GetConfiguration()+windowSize;

            if (!Results.ContainsKey(name))
            {
                Results.Add(name, new List<ChartResultViewModel>());
            }

            if (Results[name].Count(model => model.Configuration==result.Configuration)==0)
            {
                Results[name].Add(result);
            }
            _console.WriteLine("Trace "+name+" simulated");
            new ChartView(this, name).Show();
        }

        public ApplicationInstance(IList<Benchmark> benchmarks, IList<AssemblyTrace> assemblyTraces, IList<TraceGenerator> traceGenerators, IList<ScheduledAssembyTrace> scheduledTraces)
        {
            _benchmarks = benchmarks;
            _assemblyTraces = assemblyTraces;
            _traceGenerators = traceGenerators;
            _scheduledTraces = scheduledTraces;
        }

        internal void RemoveBenchmark(string name)
        {
            _benchmarks.Remove(_benchmarks.First(benchmark => benchmark.Name == name));
            _console.WriteLine("Benchmark " + name + " removed");
        }

        internal void PrepareBenchmarkForTracing(string v)
        {
            var generator = new LargeBasicBlockGenerator(_benchmarks.First(benchmark => benchmark.Name == v));
            _traceGenerators.Add(generator);
            generator.TraceGenerated += GeneratorOnTraceGenerated;
        }

        private void GeneratorOnTraceGenerated(AssemblyTrace trace)
        {
            _assemblyTraces.Add(trace);
            trace.SaveTrace();
            _console.WriteLine("Trace " + trace.Name + " completed");
            TraceGenerated?.Invoke(trace.Name, new EventArgs());
        }

        public void GenerateTraces()
        {
            foreach (var traceGenerator in _traceGenerators)
            {
                _console.WriteLine("Generating trace " + traceGenerator.Name + "...");
                traceGenerator.GenerateTrace();
            }
        }

        internal void ScheduleTrace(string traceName, string windowsize)
        {
            var windowSize = 0;

            try
            {
                windowSize = Convert.ToInt32(windowsize);

            }
            catch (FormatException)
            {
                throw new FormatException("Please insert numeric the window size!");
            }
            var scheduler = new CodeSchedulerImpl(_assemblyTraces.First(trace => trace.Name == traceName), windowSize);
            scheduler.Scheduled += SchedulingOnDone;
            scheduler.Schedule();
            _console.WriteLine("Scheduling " + traceName + "...");
        }

        private void SchedulingOnDone(ScheduledAssembyTrace scheduledTrace)
        {

            if (_scheduledTraces.Count(trace => trace.Name==scheduledTrace.Name)!=0)
            {
                var trace = _scheduledTraces.First(assembyTrace => assembyTrace.Name == scheduledTrace.Name);
                _scheduledTraces.Remove(trace);
            }

            _scheduledTraces.Add(scheduledTrace);
            scheduledTrace.SaveTrace();
            _console.WriteLine("Trace " + scheduledTrace.Name + " scheduled");
            SchedulingDone?.Invoke(scheduledTrace.Name, new EventArgs());
        }

        internal void Simulate(string traceName)
        {
            _console.WriteLine("Simulating "+traceName+"...");
            _simulator.Simulate(_scheduledTraces.First(trace => trace.Name==traceName));
        }
    }
}
