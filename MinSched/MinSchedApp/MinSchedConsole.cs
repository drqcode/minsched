﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace MinSchedApp
{
    public class MinSchedConsole
    {
        private TextBox _textBox;

        public MinSchedConsole(TextBox textBox)
        {
            _textBox = textBox;
            _textBox.IsReadOnly = true;
            _textBox.TextWrapping = TextWrapping.Wrap;
        }

        public void WriteLine(string text)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                new Action(() => _textBox.Text += text + Environment.NewLine));
        }
    }
}