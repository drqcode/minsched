﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using MinSched.Infrastructure;
using MinSched.Model.Entities.Scheduling;
using MinSched.Scheduler;

namespace MinSchedApp
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ApplicationInstance _appInstance;
        private MinSchedConsole _console;

        public MainWindow()
        {
            InitializeComponent();
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _console = new MinSchedConsole(consoleTextBox);
            _appInstance = new ApplicationInstance(_console);
            _appInstance.TraceGenerated += AppInstanceOnTraceGenerated;
            _appInstance.SchedulingDone += AppInstanceOnSchedulingDone;

            benchmarkListBox.SelectionMode = SelectionMode.Multiple;
            tracesListBox.SelectionMode = SelectionMode.Multiple;
            scheduledTracesListBox.SelectionMode = SelectionMode.Multiple;

        }

        private void AppInstanceOnSchedulingDone(object sender, EventArgs eventArgs)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                if (!scheduledTracesListBox.Items.Contains(sender))
                {
                    scheduledTracesListBox.Items.Add(sender.ToString());
                }
            }));
        }


        private void AppInstanceOnTraceGenerated(object sender, EventArgs eventArgs)
        {
            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(delegate
                {
                    if (!tracesListBox.Items.Contains(sender))
                    {
                        tracesListBox.Items.Add(sender.ToString());
                    }
                }));

            BenchmarkPanelToggle(true);
        }

        private void addBenchmarksBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFiles of = new OpenFiles(_appInstance);
            of.ShowDialog();
            if (_appInstance._benchmarks.Count == 0) return;
            benchmarkListBox.Items.Clear();
            foreach (var bench in _appInstance._benchmarks)
            {
                benchmarkListBox.Items.Add(bench.Name);
            }
        }

        private void removeBenchmarksBtn_Click(object sender, RoutedEventArgs e)
        {
            if (benchmarkListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("There is nothing to remove");
                return;
            }
            var selectedItems = benchmarkListBox.SelectedItems;

            foreach (var benchmark in selectedItems)
            {
                _appInstance.RemoveBenchmark(benchmark.ToString());
            }

            if (benchmarkListBox.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    benchmarkListBox.Items.Remove(selectedItems[i]);
            }

        }

        private void BenchmarkPanelToggle(bool toggle)
        {

            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(() => addBenchmarksBtn.IsEnabled = toggle));

            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(() => removeBenchmarksBtn.IsEnabled = toggle));

            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(() => benchmarkListBox.IsEnabled = toggle));

        }

        private void generateTraceBtn_Click(object sender, RoutedEventArgs e)
        {
            if (benchmarkListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one benchmark!");
                return;
            }

            foreach (var benchmark in benchmarkListBox.SelectedItems)
            {
                _appInstance.PrepareBenchmarkForTracing(benchmark.ToString());
            }

            BenchmarkPanelToggle(false);
            benchmarkListBox.SelectedItems.Clear();

            //GENERATE COMMAND
            _appInstance.GenerateTraces();
        }



        private void optimizeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (tracesListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one trace!");
                return;
            }
            foreach (var selectedItem in tracesListBox.SelectedItems)
            {
                try
                {
                    _appInstance.ScheduleTrace(selectedItem.ToString(), windowSizeTextBox.Text);
                }
                catch (FormatException exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }




        private void ShowResultsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void ClearResultsMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void SimulateMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (scheduledTracesListBox.SelectedItems.Count==0)
            {
                MessageBox.Show("Please select at least one scheduled trace!");
                return;
            }
            foreach (var selectedItem in scheduledTracesListBox.SelectedItems)
            {
                _appInstance.Simulate(selectedItem.ToString());
            }
        }

        private void AntiAliasCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.AntiAliasMemory);
        }

        private void AntiAliasCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.UncheckTechnique(DependencyResolverType.AntiAliasMemory);
        }

        private void MovMergingCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.MovMerging);
        }

        private void MovMergingCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.UncheckTechnique(DependencyResolverType.MovMerging);
        }

        private void ImmediateMergingCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.ImmediateMerging);
        }

        private void ImmediateMergingCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.UncheckTechnique(DependencyResolverType.ImmediateMerging);
        }

        private void MovReabsorptionCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.CheckTechnique(DependencyResolverType.MovReabsorption);
        }

        private void MovReabsorptionCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            TechniquePool.Instance.UncheckTechnique(DependencyResolverType.MovReabsorption);
        }

        private void OpenTraceMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (tracesListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a trace!");
                return;
            }
            var asmTraceName = tracesListBox.SelectedItems[0].ToString();
            System.Diagnostics.Process.Start(UserFileManager.Instance.GetFilePath(asmTraceName + ".btrc"));
        }

        private void SaveTraceMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (tracesListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a trace!");
                return;
            }
            foreach (var selectedItem in tracesListBox.SelectedItems)
            {
                _appInstance._assemblyTraces.First(trace => trace.Name == selectedItem.ToString()).SaveTrace();
            }
        }

        private void tracesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            scheduledTracesListBox.SelectedItems.Clear();
            benchmarkListBox.SelectedItems.Clear();
        }

        private void scheduledTracesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            benchmarkListBox.SelectedItems.Clear();
            tracesListBox.SelectedItems.Clear();
        }

        private void benchmarkListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tracesListBox.SelectedItems.Clear();
            scheduledTracesListBox.SelectedItems.Clear();
        }

        private void tracesListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }

        private void OpenScheduledTraceMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (scheduledTracesListBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select a scheduled trace!");
                return;
            }
            var asmTraceName = scheduledTracesListBox.SelectedItems[0].ToString();
            System.Diagnostics.Process.Start(UserFileManager.Instance.GetFilePath(asmTraceName + ".strc"));
        }
    }
}
