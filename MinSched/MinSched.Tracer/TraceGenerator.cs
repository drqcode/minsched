﻿using System;
using System.IO;
using System.Threading;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Entities.Scheduling;

namespace MinSched.Tracer
{
    public delegate void TraceGeneratedEventHandler(AssemblyTrace trace);
    public abstract class TraceGenerator
    {
        private readonly string _name;

        public string Name => _name;

        protected Thread TracerThread;
        protected Benchmark Benchmark;

        public event TraceGeneratedEventHandler TraceGenerated;

        protected TraceGenerator(Benchmark benchmark)
        {
            Benchmark = benchmark;
            _name = Benchmark.Name;
        }

        public abstract void GenerateTrace();

        protected virtual void OnGeneratedTrace(AssemblyTrace trace)
        {
            TraceGenerated?.Invoke(trace);
        }
    }
}
