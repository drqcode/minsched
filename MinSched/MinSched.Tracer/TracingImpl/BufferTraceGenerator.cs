﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;

namespace MinSched.Tracer.TracingImpl
{
    public class BufferTraceGenerator : TraceGenerator
    {
        private string _clearInsFileName;
        private string[] _insBuffer;
        private IList<TracedBranch> _trcBuffer;


        public BufferTraceGenerator(Benchmark benchmark) : base(benchmark)
        {
        }

        public override void GenerateTrace()
        {

            MakeClearIns(Path.ChangeExtension(Benchmark.Name,".ins"));
            LoadInsToBuffer(_clearInsFileName);
            //LoadTrcFileToBuffer(TrcFileName);
            MakeBtrc();
        }

        private bool TestLine(string line)
        {
            line = Regex.Replace(line, @"\s+", "");
            line =  Regex.Replace(line, @"\t+", "");
            if (line.StartsWith(".") || line.EndsWith(":") || line == "" || line.StartsWith("/") )
                return false;
            return true;
        }

        private void MakeClearIns(string insFileName)
        {
            _clearInsFileName = "ClearFile.ins";
            StreamReader sr = new StreamReader(insFileName);
            StreamWriter sw = new StreamWriter(_clearInsFileName);
            string lineFeed;
            int ct=0;

            while ((lineFeed = sr.ReadLine()) != null)
            {
                if (TestLine(lineFeed))
                {
                    sw.WriteLine(lineFeed);
                    ct++;
                }
            }

            _insBuffer = new string[ct];

            sr.Close();
            sw.Close();

        }
        private void LoadInsToBuffer(string clearInsFileName)
        {
            StreamReader sr = new StreamReader(clearInsFileName);
            string lineFeed;
            int i;
            for (i = 0; (lineFeed = sr.ReadLine()) != null; i++)
            {
                _insBuffer[i] = lineFeed;
            }
            sr.Close();
        }

        private void LoadTrcFileToBuffer(string trcFileName)
        {
            StreamReader sr = new StreamReader(trcFileName);
            string lineFeed;
            int i;
            _trcBuffer = new List<TracedBranch>();
            while ((lineFeed = sr.ReadLine()) != null)
            {
                string[] splited = lineFeed.Split(' ');
                bool ok;
                for (i = 0; i < splited.Length; i++)
                {
                    ok = true;
                    if (splited[i] == "")
                    {
                        for (int j = i; j < splited.Length - 1; j++)
                        {
                            splited[j] = splited[j + 1];
                            if (splited[j + 1] != "")
                                ok = false;
                        }
                        if (!ok)
                             i--;
                    }
                }

                for (i = 0; i < splited.Length; i++)
                {
                    if (splited[i]=="B")
                    {
                        _trcBuffer.Add(new TracedBranch(Convert.ToInt32(splited[i + 1]),
                            Convert.ToInt32(splited[i + 2])));
                    }
                }
            }
            sr.Close();
        }


            

        private void MakeBtrc()
        {
            StreamWriter sw = new StreamWriter("Output.btrc");
            int i,pc = 0;
            foreach (var branch in _trcBuffer)
            {
                for (i=pc;i<branch.StartAddress || i<_insBuffer.Length;i++)
                    sw.WriteLine(_insBuffer[i]);
                pc = branch.JumpAddress;
            }
            sw.Close();
         
        }
    }
}
