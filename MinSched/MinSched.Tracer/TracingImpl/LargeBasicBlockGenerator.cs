﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MinSched.Model.DomainBuilders;
using MinSched.Model.Entities;
using MinSched.Model.Entities.Benchmark;
using MinSched.Model.Entities.Scheduling;
using MinSched.Model.Interface;

namespace MinSched.Tracer.TracingImpl
{
    public class LargeBasicBlockGenerator : TraceGenerator
    {
        public LargeBasicBlockGenerator(Benchmark benchmark) : base(benchmark)
        {
        }


        public override void GenerateTrace()
        {
            TracerThread = new Thread(Trace);
            TracerThread.Start();
        }

        private void Trace()
        {
            var traceBuilder = new AssemblyTraceBuilder();
            var branches = Benchmark.BranchTrace.GetNearTargetBranches(Benchmark.AssemblyCode.LastAddress);

            traceBuilder.AppendBlock(Benchmark.AssemblyCode.GetBasicBlock(0,branches[0].StartAddress));
            for (var i = 0; i < branches.Count-1; i++)
            {
                traceBuilder.AppendBlock(Benchmark.AssemblyCode.GetBasicBlock(branches[i].JumpAddress, branches[i+1].StartAddress));
            }
            traceBuilder.AppendBlock(Benchmark.AssemblyCode.GetBasicBlock(branches[branches.Count-1].JumpAddress,Benchmark.AssemblyCode.ExitAddress));
            
            OnGeneratedTrace(new AssemblyTrace(Benchmark.Name,traceBuilder));
        }
    }
}